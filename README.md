# Schillix
*Kontaktadresse: joerg@schily.net*

Documentation and [Schillix](https://www.berlios.de/software/schillix/) distribution used during the course [Systemverwalter](kvv-link) in September 2019

This repository contains
* ISO image used for the initial boot and installation of Schillix 0.8 (Build 147)
* tar archives of various tools and their supporting ecospheres added in the meantime
   * `/usr/gnu`  Gnu ecosystem - to extract `cd /;star -zxp -no-secure-links gnu.tar.gz`
   * `/usr/sfw`  Sunfreeware ecosystem - `cd /; star -zxp sfw.tar.gz`)
   * `/usr/X11`  Sunfreeware ecosystem - `cd /; star -zxp X11.tar.gz`)
   * `/opt/csw`  csw ecosystem, `cd /opt; star -zxp < csw.tar.gz`
   * `/opt/gcc-3-4-3` C Compiler
   * `/opt/schily` Snapshot of 2019-08-13 Schilytools.
* [Installation Instructions](/../wikis/Installation/Overview)

## Note
During the course, Schillix is installed into [VMware](https://www.vmware.com) virtual machines (VM). 
The keyboard handling of VMware doesn't allow entering the characters to proceed 
with the installation when Schillix default locale of DE is used, therefore the 
bootimage provided for the course jumps right into the selection of the keyboard 
locale, and allows to select US-Ascii.

The image otherwise does not differ from the one available on 
[sourceforge](https://sourceforge.net/projects/schillix/files/)
